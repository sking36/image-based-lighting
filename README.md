# Image Based Rendering
This project is collabrated by Fan Feng and Ziqing(Kelly) Wang.

## Description
In this project, we provided the pipeline for image based rendering. We used a mirror ball to capture lighting information in real world. With sequences of low dynamic range photos, we can obtain a high dynamic range photo. By transforming the HDR photo into panorama, then we can use it in 3D rendering software to light the scene and insert synthetic object into it seamlessly. This readme document will walk you through the entire process.

## Tool used
1. A mirror ball
2. A DSLR camera, tripod
3. A photo editing software to trim photos
4. Maya with Arnold

## Input
1. A sequence of low dynamic range photos
2. If removing the photographer/tripod, a set of LDR images required from a different angle
3. A background image

## output
1. A rendered image with 3D object inserted

## Walk-through
**Set Up**

1. Please use vckpkg to install Eigen3. Use the following commands to install vcpkg and Eigen3.

        cd "C:\Program Files"
        git clone https://github.com/microsoft/vcpkg.git
        .\vcpkg\bootstrap-vcpkg.bat
        cd "C:\Program Files\vcpkg"
        .\vcpkg.exe search Eigen
        .\vcpkg.exe install eigen3:x64-windows

2. Use CMake to build our project.
        
        Add entry for vcpkg to let  CMake find the tool
<img src="images/cmake1.png" width="1000">

        Click configure and you will see the following.
<img src="images/cmake2.png" width="1000">

        You should see the configured variables.
<img src="images/cmake3.png" width="1000">
        
        Click generate and you will be able to open the .sln file in your build folder.

**Make HDR**
1. Take a sequence of photos of a mirror ball in the desired environment with a range of exposure time. If removing photographer/tripod, then a set of photos is needed from a different angle. Ideally, photos from the same angle should align perfectly. Also, take a background photo that synthetic objects will be inserted into. 
<img src="images/ldr1.jpg" width="1000">
<img src="images/ldr2.jpg" width="1000">

2. Put photos into the input folder and run program part 1. It will create two HDR photos.
3. Manually trim the photos into squares that frame the mirror ball sphere using a photo editing software such as photoshop or gimp.

<img src="images/imHDR1-tonemapped.jpg" width="400">
<img src="images/imHDR2-tonemapped.jpg" width="400">

<em>The HDR images are tonemapped for display</em>

**Panoramic transformation**

1. Run program part 2. It will unwrap the 2 HDR photos into panoramas.
<img src="images/imHDR1-eq-tonemapped.jpg" width="800">
<img src="images/imHDR2-eq-tonemapped.jpg" width="800">

<em>The HDR images are tonemapped for display</em>

**Photographer/tripod removal**
1. Open click.html and manually select an anchor point on both photos(image file dir should be changed)
2. Then copy the x values into main.cpp.
3. Use photo editing software to draw a mask with the same dimension as the HDR square photos. Use black as the background, and white as the area which the photographer/tripod presented in the first mirror ball HDR photo. Save the mask into the input folder.
<img src="images/tripod_mask.jpg" width="400">

4. Run program part 3. The output will be an unwrapped HDR map removing the photographer/tripod.
<img src="images/removeTripodHDRIm-tonemapped1.jpg" width="800">

<em>The HDR images are tonemapped for display</em>

**Render and composite**
1. Use the unwrapped HDR map created in the last step in a 3D software such as Maya or Blender. We used Maya rendered with Arnold. Create an aiSkydome Light in the scene and select the HDR map as the color input.
2. Then create a plane that matches the perspective of the background photo as the “ground” that 3D models will sit on. Manipulate the model’s position, materials into the desired effect.
3. Rotate the skydome and change the light intensity if needed.
4. Render 3 images from the perspective matching the background photo:
    1. Objects and plane
    <img src="images/render.jpg" width="600">

    2. Only the plane
    <img src="images/empty.jpg" width="600">

    3. Objects mask/alpha channel
    <img src="images/mask.jpg" width="600">

5. Put render images into the input folder and run program part 4. Change the value of parameter c if needed, which is the intensity of the shadow. The final photo with synthetic objects inserted will be created.
6. Alternatively, one can use [aiShadowMatte](https://docs.arnoldrenderer.com/display/A5AFMUG/Shadow+Matte) shader for the plane in Maya scene. A final image with shadow can be obtained without compositing using code.

## More Results
<img src="data/input/anim/render00.jpg" width="600">
<img src="data/input/anim/render01.jpg" width="600">
<img src="data/input/anim/render02.jpg" width="600">
<img src="data/input/anim/render03.jpg" width="600">
<img src="data/input/anim/render04.jpg" width="600">

## Contribution
Fan Feng contributes to: project setup, panoramic transformation and make HDR.

Kelly Wang contributes to: make HDR, photographer/tripod removal and render and composite.

## Acknowledgement
The project is based on CS498 computational photography in University of Illinois Urbana-Champaign 2014 Fall: https://courses.engr.illinois.edu/cs498dh3/fa2014/projects/ibl/ComputationalPhotography_ProjectIBL.html

The high dynamic range part of the code is based on assignment five of COSC73/273 Computational Photography in Dartmouth College 2020 Fall.

There are references materials under folder references too.
