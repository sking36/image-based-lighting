#pragma once

#include "panoramic.h"
#include <cassert>
#include <iostream>
#include <Eigen/Dense>
#include "utils.h"
#include <vector>
#define PI		3.14159265358979323846

using namespace Eigen;

FloatImage panoramicTransform(FloatImage& sphere_image) {
	
	FloatImage sphere_domain = calcSphereDomain(sphere_image);
	FloatImage equirectangular_domain=calcEquirectangularDomain(sphere_image);
	FloatImage out(equirectangular_domain.width(),equirectangular_domain.height(),sphere_image.channels());
	
	//mark that the pixel hasn't been filled
	for (int i = 0; i < out.width(); i++) {
		for (int j = 0; j < out.height(); j++) {
			for (int c = 0; c < out.channels(); c++) {
				out(i,j,c) = -1.f;	
			}
		}
	}

	//fill pixels already on the sphere domain
	float radius = sphere_domain.width()/2.f;
	for (int i = 0; i < sphere_domain.width(); i++) {
		for (int j = 0; j < sphere_domain.height(); j++) {
			if (distToCenter(i,j,radius) > radius) {continue;}

			float theta = sphere_domain(i,j,0);
			float phi= sphere_domain(i,j,1);
			int x = (int) (theta/2.f/PI*equirectangular_domain.width());
			int y = (int) (phi/PI*equirectangular_domain.height());
			x=clamp(x,0,equirectangular_domain.width()-1);
			y=clamp(y,0,equirectangular_domain.height()-1);
			for (int c = 0; c < sphere_image.channels(); c++) {
				out(x,y,c) = sphere_image(i,j,c);
			}
		}
	}

	// interpolate pixel value
	for (int i = 0; i < out.width(); i++) {
		for (int j = 0; j < out.height(); j++) {
			if (out(i, j, 0) < 0.f) {
				std::vector<float> pixel = interpolate(i,j,out);
				for (int c = 0; c < out.channels(); c++) {
					out(i,j,c) = pixel[c];
				}
			}
		}
	}
	
	return out;
}

FloatImage calcSphereDomain(FloatImage& sphere_image) {
	FloatImage out(sphere_image.width(),sphere_image.height(),3);
	float center = sphere_image.width()/2.f;
	for (int i = 0; i < out.width(); i++) {
		for (int j = 0; j < out.height(); j++) {
			float dist = distToCenter(i,j,center);
			float R = center; // not normalized radius
			if (dist<R) {
				//rhs coordinate
				Vector3f view_direction(0.f,0.f,-1.f);
				Vector3f point(i+0.5f-center,j+0.5f-center,sqrt(pow(R,2)-pow(dist,2)));
				Vector3f normal = point.normalized();
				Vector3f reflection = view_direction - 2.f*view_direction.dot(normal)*normal;
				reflection.normalize();
				// x=cos(theta-pi/2)sin(phi), -y=cos(phi)
				// phi is latitude and theta is longitude
				float phi = acos(-reflection[1]);
			
				// acos cannot deal with value outside of 1.f and -1.f
				float theta = acos(clamp(reflection[0]/sin(phi),-1.f,1.f));

				if (reflection[2]>0.f) {
					if (reflection[0] < 0) {
						theta=theta-(float)PI/2.f;
					}
					else {
						theta=theta+3.f*(float)PI/2.f;
					}
				}else{
					theta=3.f*(float)PI/2.f-theta;
				}

				out(i,j,0) = theta;
				out(i,j,1) = phi;
				out(i,j,2) = 0.f;
			}
		}
	}
	return out;
}

FloatImage calcEquirectangularDomain(FloatImage& sphere_image) {
	FloatImage equirectangular_domain(sphere_image.width()*2, sphere_image.height(), sphere_image.channels());
	for (int i = 0; i < equirectangular_domain.width(); i++) {
		for(int j=0;j<equirectangular_domain.height();j++){
			float theta = (i+0.5f)/equirectangular_domain.width()*2*PI;
			float phi = (j+0.5f)/equirectangular_domain.height()*PI;
			equirectangular_domain(i,j,0)=theta;
			equirectangular_domain(i,j,1)=phi;
		}
	}
	return equirectangular_domain;
}

std::vector<float> interpolate(int x, int y, FloatImage& im, int num_neighbors) {
	std::vector<Vector2i> neighbors;
	
	int count = 1;
	// find neighbors
	while (neighbors.size() < num_neighbors) {
		for (int i = -count; i < count+1; i++) {
			if(x+i>=im.width()||x+i<0){continue;}
			
			if (y - count >= 0) {
				if (im(x + i, y - count, 0) >= 0.f) {
					neighbors.push_back(Vector2i(x+i,y-count));
				}
			}
			
			if (y + count < im.height()) {
				if (im(x + i, y + count, 0) >= 0.f) {
					neighbors.push_back(Vector2i(x+i,y+count));
				}
			}
		}

		for (int j = -count; j < count+1; j++) {
			if(y+j>=im.height()||y+j<0){continue;}
			
			if (x - count >= 0) {
				if (im(x - count, y + j, 0) >= 0.f) {
					neighbors.push_back(Vector2i(x-count,y+j));
				}
			}
			
			if (x + count < im.width()) {
				if (im(x + count, y + j, 0) >= 0.f) {
					neighbors.push_back(Vector2i(x+count,y+j));
				}
			}
		}
		count+=1;
	}

	std::vector<float> weights(neighbors.size());
	float weight_sum=0.f;
	for (int i = 0; i < neighbors.size(); i++) {
		float inverse_distance = 1.f/(neighbors[i] - Vector2i(x,y)).norm();
		weights[i]=inverse_distance;
		weight_sum+=inverse_distance;
	}

	std::vector<float> out(im.channels(),0.f);
	for (int i = 0; i < weights.size(); i++) {
		weights[i] = weights[i]/weight_sum;
		for (int c = 0; c < im.channels(); c++) {
			out[c] += weights[i] * im(neighbors[i][0],neighbors[i][1],c);
		}
	}
	
	return out;
}

inline float distToCenter(int x, int y, float center) {
	return sqrt(pow(x+0.5f-center,2)+pow(y+0.5f-center,2));
}