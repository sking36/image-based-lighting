#include "panoramic.h"
#include "composite.h"
#include "hdr.h"
#include <sstream>

using namespace std;

void testMakeAndToneMap_ante2()
{
	// load images
	vector<FloatImage> imSeq;
	imSeq.push_back(FloatImage(DATA_DIR "/input/a5/ante2-1.png"));
	imSeq.push_back(FloatImage(DATA_DIR "/input/a5/ante2-2.png"));

	// create hdr image
	FloatImage hdr = makeHDR(imSeq);

	// save out HDR image
	hdr.write(DATA_DIR "/output/ante2-out.hdr");

	// tone map with gaussian blur
	toneMap(hdr, 100, 1, false).write(DATA_DIR "/output/ante2-tonedHDRsimple-gauss.png");
	// tone map with bilaterial
	toneMap(hdr, 100, 3, true, 0.1).write(DATA_DIR "/output/ante2-tonedHDRsimple-bilateral.png");
}

void testCalcSphereDomain(){
	FloatImage im1(DATA_DIR "/input/mirror-ball1.jpg");
	FloatImage sphere_domain = calcSphereDomain(im1);
	sphere_domain.write(DATA_DIR "/output/reflection-sphere-domain1.jpg");
}

void testPanoramicTransform() {
	FloatImage im1(DATA_DIR "/input/mirror-ball2.jpg");
	FloatImage equirectangular_domain = panoramicTransform(im1);
	equirectangular_domain.write(DATA_DIR "/output/equirectangular-domain2.jpg");
}

void testRemoveTripod() {
	FloatImage im1(DATA_DIR "/input/mirror1_1.jpg");
	FloatImage im2(DATA_DIR "/input/mirror1_2.jpg");
	FloatImage mask(DATA_DIR "/input/mask.jpg");
	FloatImage removeTripodIm = removeTripod(im1, im2, mask, 642, 40);
	removeTripodIm.write(DATA_DIR "/output/removeTripodIm.jpg");
}

void testComposite() {
	FloatImage imM(DATA_DIR "/input/mask.jpg");
	FloatImage imI(DATA_DIR "/input/background.jpg");
	FloatImage imE(DATA_DIR "/input/empty.jpg");
	FloatImage imR(DATA_DIR "/input/render.jpg");
	FloatImage compositeIm = composite(imM, imR, imE, imI, 1.0f);
	compositeIm.write(DATA_DIR "/output/compositeIm.jpg");
}

void testIBL_part1(string folder, int nImages, bool removeCam) {
	
	std::vector<FloatImage> imSeq1, imSeq2;
	
	// add each image to the vector of images
	for (int i = 1; i <= nImages; i++)
	{
		ostringstream ss,ss1;
		ss << DATA_DIR "/input/" << folder << "/ldr1-" << i << ".jpg";
		string filename = ss.str();
		imSeq1.push_back(FloatImage(filename));
		if(removeCam)
		{
			ss1 << DATA_DIR "/input/" << folder << "/ldr2-" << i << ".jpg";
			filename = ss1.str();
			imSeq2.push_back(FloatImage(filename));
		}
		
	}
	
	FloatImage imHDR1 = makeHDR(imSeq1);
	imHDR1.write(DATA_DIR "/input/"+folder+"/imHDR1.hdr");
	if(removeCam)
	{
		FloatImage imHDR2 = makeHDR(imSeq2);
		imHDR2.write(DATA_DIR "/input/"+folder+"/imHDR2.hdr");
	}
	//trim the photo
}

void testIBL_part2(string folder, bool removeCam) {
	FloatImage imHDR1(DATA_DIR "/input/"+folder+"/imHDR1.hdr");
	FloatImage imHDR2;
	if(removeCam)
	{imHDR2 = FloatImage(DATA_DIR "/input/"+folder+"/imHDR2.hdr");}
	//Panoramic Transform
	FloatImage equirectangular_domain1 = panoramicTransform(imHDR1);
	equirectangular_domain1.write(DATA_DIR "/input/"+folder+"/imHDR1-equirectangular.hdr");
	equirectangular_domain1.write(DATA_DIR "/input/"+folder+"/imHDR1-equirectangular.jpg");
	if(removeCam)
	{
	FloatImage equirectangular_domain2 = panoramicTransform(imHDR2);
	equirectangular_domain2.write(DATA_DIR "/input/"+folder+"/imHDR2-equirectangular.hdr");
	equirectangular_domain2.write(DATA_DIR "/input/"+folder+"/imHDR2-equirectangular.jpg");
	}
	//Manually select anchor points in click.html
}

//Remove Camera
void testIBL_part3(string folder, bool removeCam) {
	FloatImage im1(DATA_DIR "/input/"+folder+"/imHDR1-equirectangular.hdr");
	FloatImage im2,mask,removeTripodIm;
	if(removeCam)
	{
		im2 = FloatImage(DATA_DIR "/input/"+folder+"/imHDR2-equirectangular.hdr");
		mask = FloatImage(DATA_DIR "/input/"+folder+"/tripod_mask.jpg");
		//replace the parameters with anchors points
		removeTripodIm = removeTripod(im1, im2, mask, 1066.72, 415.36);
		removeTripodIm.write(DATA_DIR "/input/"+folder+"/removeTripodHDRIm.hdr");
	}	
}

//Composite
void testIBL_part4(string folder) {
	FloatImage imM(DATA_DIR "/input/"+folder+"/mask.jpg");
	FloatImage imI(DATA_DIR "/input/"+folder+"/background.jpg");
	FloatImage imE(DATA_DIR "/input/"+folder+"/empty.jpg");
	FloatImage imR(DATA_DIR "/input/"+folder+"/render.jpg");
	FloatImage compositeIm = composite(imM, imR, imE, imI, 1.8f);
	compositeIm.write(DATA_DIR "/output/"+folder+"/compositeIm.jpg");
}

void testInterpolate(int num, int numFrames) {
	char buffer[255];
	FloatImage start(DATA_DIR "/input/interpolate/imHDR1-equirectangular.hdr");
	FloatImage end(DATA_DIR "/input/interpolate/imHDR2-equirectangular.hdr");
	std::vector<FloatImage> imInterpolate = interpolateIm(start, end, numFrames);
	for(int n = 0; n < imInterpolate.size(); n++){
		int index = n;
		FloatImage im = imInterpolate[n];
		sprintf(buffer, DATA_DIR "/input/interpolate/hdrmap_interpolate.%03d.hdr", index);
		im.write(buffer);
	}
}

void batchIBL(int num){
	char folder[255];
	int nImages = 3;
	for(int i = 0; i < num; i++){
		sprintf(folder, "anim/p%02d", i+1);
		// testIBL_part1(folder, nImages, false);
		// testIBL_part2(folder, false);
	}
}

void batchComposite(){
	char buffer[255];
	for(int i = 1; i < 25; i++){
		FloatImage imM(DATA_DIR "/input/interpolate/mask.jpg");
		sprintf(buffer, DATA_DIR "/input/interpolate/background/interpolate_1.%04d.jpg", i);
		FloatImage imI(buffer);
		sprintf(buffer, DATA_DIR "/input/interpolate/empty/empty.%04d.jpg", i);
		FloatImage imE(buffer);
		sprintf(buffer, DATA_DIR "/input/interpolate/render/interpolate_1.%04d.jpg", i);
		FloatImage imR(buffer);
		FloatImage compositeIm = composite(imM, imR, imE, imI, 1.8f);
		sprintf(buffer, DATA_DIR "/output/interpolate/compositeIm.%04d.jpg", i);
		compositeIm.write(buffer);
	}
}

void tonemapForDisplay(string folder)
{
	FloatImage im(DATA_DIR "/input/"+folder+"/imHDR2.hdr");
	FloatImage tone = toneMap(im,20,1.5);
	tone.write(DATA_DIR "/input/"+folder+"/imHDR2-tonemapped.jpg");
}

int main()
{
	string folder = "interpolate";						//change folder name
	int nImages = 3;
	////Test individual components
	// testMakeAndToneMap_ante2();
	// testCalcSphereDomain();
	// testPanoramicTransform();
	// testRemoveTripod();
	// testComposite();

	// string folder = "p3";					//change folder name
	// int nImages = 3;
	// testIBL_part1(folder, nImages, false);	//Get 2 HDR photos
	// testIBL_part2(folder, false);			//Remove tripod and get HDR map for rendering in MAYA
	// testIBL_part3(folder, nImages);			//Output final image
	// testIBL_part4(folder);					//Output final image

	////Batch IBL for interpolation ibl
	// folder = "anim";		//change folder name
	// testInterpolate(5, 24);
	// batchIBL(5);
	// batchComposite();

	////tonemapping
	// tonemapForDisplay(folder);
}
