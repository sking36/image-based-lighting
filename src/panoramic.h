#pragma once

#include "floatimage.h"
#include <vector>

// convert mirror ball image to equirectangular domain
FloatImage panoramicTransform(FloatImage& sphere_image);

// return a two channel FloatImage. The first channel is the phi value 
// and the second channel is the theta value of the equirectangular domain
FloatImage calcEquirectangularDomain(FloatImage& sphere_image);

// return a two channel FloatImage. The first channel is the phi value 
// and the second channel is the theta value of the reflection on that pixel 
FloatImage calcSphereDomain(FloatImage& sphere_image);

// interpolate with inverse distance
std::vector<float> interpolate(int x, int y, FloatImage& im, int num_neighbors=3);

inline float distToCenter(int x, int y, float center);