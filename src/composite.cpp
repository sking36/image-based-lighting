#pragma once

#include "floatimage.h"
#include "panoramic.h"
#include "composite.h"
#include <iostream>
#include "utils.h"

//modified from roll/align.cpp in a3
FloatImage panoramicRotation(FloatImage& panoIm, int shift)
{
    int xNew;
	FloatImage imRoll(panoIm.width(), panoIm.height(), panoIm.channels());

	// for each pixel in the original image find where its corresponding
	// location is in the rolled image
	for (int x = 0; x < panoIm.width(); x++)
	{
		for (int y = 0; y < panoIm.height(); y++)
		{
			// use modulo to figure out where the new location is in the
			// rolled image. Then take care of when this returns a negative number
			xNew = (x + shift) % panoIm.width();
			xNew = (xNew < 0) * (imRoll.width() + xNew) + (xNew >= 0) * xNew;

			// assign the rgb values for each pixel in the original image to
			// the location in the new image
			for (int z = 0; z < panoIm.channels(); z++)
				imRoll(xNew, y, z) = panoIm(x, y, z);
		}
	}

	// return the rolled image
	return imRoll;
}

//Use already panoramic transformed images and a polar mask to compute a result hdr map removing the tripod
FloatImage removeTripod(FloatImage& im1, FloatImage& im2, FloatImage& maskPolar, int x1, int x2)
{
	//panoramize mask
	FloatImage mask = panoramicTransform(maskPolar);

    if (im1.width()!=im2.width() || im1.width()!=mask.width() ||
        im1.height()!=im2.height() || im1.height()!=mask.height())
		throw MismatchedDimensionsException();
    
    FloatImage out = im1;
    //panorama rotation on Im1 to align the second
    FloatImage panoIm1 = panoramicRotation(im1, x2-x1);
	FloatImage panoIm2 = im2;

    //blend two panorama images to remove the tripod/photographer
    for (int x = 0; x < panoIm1.width(); x++)
	{
		for (int y = 0; y < panoIm1.height(); y++)
		{
            for (int z = 0; z < panoIm1.channels(); z++)
		    {
                out(x, y, z) = mask(x, y) * panoIm1(x, y, z) + (1-mask(x, y)) * panoIm2(x, y, z);
            }
        }
    }

    return out;
}

//composite = M.*R + (1-M).*I + (1-M).*(R-E).*c
FloatImage composite(FloatImage& M, FloatImage& R, FloatImage& E, FloatImage& I, float c)
{
    FloatImage out = I;
    for (int x = 0; x < I.width(); x++)
	{
		for (int y = 0; y < I.height(); y++)
		{
            for (int z = 0; z < I.channels(); z++)
		    {
                out(x,y,z) = M(x,y,z)*R(x,y,z) + (1-M(x,y,z))*I(x,y,z) + (1-M(x,y,z))*(R(x,y,z)-E(x,y,z))*c;
            }
        }
    }

    return out;
}

//interpolate two images with given frame numbers
std::vector<FloatImage> interpolateIm(FloatImage start, FloatImage end, int numFrames){
	// check if im1 and im2 are the same size and throw the appropriate exception before performing morph
	if (start.sizeX() != end.sizeX() || start.sizeY() != end.sizeY())
		throw MismatchedDimensionsException();

	std::vector<FloatImage> outputIms;
	
	float step = 1/(float)(numFrames+1);
	FloatImage output(start);
	outputIms.push_back(start);

	float t = step;
	int n = 0;
	for(; n < numFrames; t+=step, n++){
		for (int x = 0; x < start.width(); x++)
		{
			for (int y = 0; y < start.height(); y++)
			{
				for (int z = 0; z < start.channels(); z++)
				{
					output(x,y,z) = (1-t)*start(x,y,z)+t*end(x,y,z);
				}
			}
		}
		outputIms.push_back(output);
	}
	return outputIms;
}