#pragma once

#include "floatimage.h"
#include "panoramic.h"
#include <iostream>
#include "utils.h"

FloatImage panoramicRotation(FloatImage& panoIm, int shift);

FloatImage removeTripod(FloatImage& panoIm1, FloatImage& panoIm2, FloatImage& mask, int x1, int x2);

FloatImage composite(FloatImage& M, FloatImage& R, FloatImage& E, FloatImage& I, float c=1.0f);

std::vector<FloatImage> interpolateIm(FloatImage start, FloatImage end, int numFrames);